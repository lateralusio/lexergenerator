package io.lateralus.lexergenerator.main;

public class LexerGeneratorException extends Exception {

	public LexerGeneratorException(String message, Throwable cause) {
		super(message, cause);
	}
}
